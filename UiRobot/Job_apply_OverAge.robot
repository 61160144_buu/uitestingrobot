*** Settings ***
Library    SeleniumLibrary

*** Variables ***


*** Test Cases ***
คุณวรวิทย์ค้นหางาน Programmer และสมัครงานสำเร็จ
    เข้าเว็บไปที่หน้าค้นหา
    พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    หน้าเว็บจะขึ้นอาชีพ Programmer 4 ตัว
    เลือกตำแหน่งงาน Senior Java Programmer
    กดสมัคร
    ระบบแสดงผลลัพธ์ว่า อายุไม่อยู่ในเกณฑ์ที่กำหนด

*** Keywords ***
เข้าเว็บไปที่หน้าค้นหา
    Open Browser    http://localhost:3000/searchJob    chrome
    Set Selenium Speed    0.5

พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    Input Text    id=search_text    Programmer
    Click Element    id=search_button

หน้าเว็บจะขึ้นอาชีพ Programmer 4 ตัว
    Element Text Should Be    id=name_2    Senior Java Programmer

เลือกตำแหน่งงาน Senior Java Programmer
    Click Element    id=show_detail_2
    Wait Until Element Contains    id=name    Senior Java Programmer

กดสมัคร
    Click Element    id=apply_job

ระบบแสดงผลลัพธ์ว่า อายุไม่อยู่ในเกณฑ์ที่กำหนด
    Element Should Contain    id=message    Age out of range
